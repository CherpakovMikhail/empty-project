import sys


class Point:
    x: int
    y: int
    visited: bool

    def __init__(self, x_, y_):
        self.x = x_
        self.y = y_
        self.visited = False

    def __eq__(self, other):
        if other.__class__ is not self.__class__:
            return NotImplemented
        return (self.x, self.y) == (other.x, other.y)


def read_file(filename):
    matrix = []
    with open(filename) as f:
        for line in f:
            matrix.append([int(x) for x in line.split()])
    return matrix


def shape(m):
    return len(m), len(m[0])


def possible_neighbours(point, len_x, len_y):
    x, y = point.x, point.y
    return [Point(x, y + 1), Point(x + 1, y),
            Point(x, y - 1), Point(x - 1, y)]


def check_neighbours_on_table(neighbours, len_x, len_y):
    new_neighbours = neighbours.copy()
    for neighbor in neighbours:
        if neighbor.x < 0 or neighbor.x == len_x or \
                neighbor.y < 0 or neighbor.y == len_y:
            new_neighbours.remove(neighbor)
    return new_neighbours


def find_island_in_list(matrix, points, cur_point, len_x,
                        len_y, visited_points, res_points):
    visited_points.append(cur_point)
    neighbours = possible_neighbours(cur_point, len_x, len_y)
    neighbours = check_neighbours_on_table(neighbours, len_x, len_y)
    for neighbor in neighbours:
        if not matrix[neighbor.x][neighbor.y]:
            continue
        if matrix[neighbor.x][neighbor.y] == 1 and \
                neighbor not in visited_points:
            res_points.remove(neighbor)
            find_island_in_list(matrix, neighbours, neighbor, len_x,
                                len_y, visited_points, res_points)

    return


def find_all_islands(matrix):
    len_x, len_y = shape(matrix)
    points = []
    for i in range(len_x):
        for j in range(len_y):
            if matrix[i][j]:
                points.append(Point(i, j))
    res_points = points.copy()
    visited_points = []

    for point in points:
        find_island_in_list(matrix, points, point, len_x,
                            len_y, visited_points, res_points)

    return len(res_points)


def main():
    filename = sys.argv[-1]
    matrix = read_file(filename)
    print(find_all_islands(matrix))


if __name__ == '__main__':
    main()
