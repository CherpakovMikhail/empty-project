import sys


class Edge:

    def __init__(self, node1, node2, weight):
        self.node1 = node1
        self.node2 = node2
        self.weight = weight


class MySet:

    def __init__(self, obj_name, set_index):
        self.obj_name = obj_name
        self.index = set_index


parents = []
ranks = []


def init_set(list_nodes):
    list_objects = []
    for i in range(len(list_nodes)):
        ob = MySet(list_nodes[i], i)
        list_objects.append(ob.obj_name)
        parents.append(0)
        ranks.append(0)
        make_set(i)

    return list_objects


def make_set(i):
    parents[i] = i
    ranks[i] = 0


def find(i):
    if i != parents[i]:
        parents[i] = find(parents[i])
    return parents[i]


def union(i, j):
    i_id = find(i)
    j_id = find(j)
    if i_id == j_id:
        return False

    if ranks[i_id] > ranks[j_id]:
        parents[j_id] = i_id
    else:
        parents[i_id] = j_id
        if ranks[i_id] == ranks[j_id]:
            ranks[j_id] += 1
    return True


def read_file(filename):
    edges = []
    with open(filename) as file:
        list_nodes = file.readline().strip().split()
        list_odj = init_set(list_nodes)

        for line in file:
            node1, node2, weight = line.split()
            edges.append(Edge(node1, node2, weight))
    return edges, list_odj


def findTree(edges, list_obj):
    edges.sort(key=lambda x: x.weight)
    size_tree = 0
    i = 0
    result_set = []
    while size_tree != len(parents) and i < len(edges) - 1:
        node1_set_index = list_obj.index(edges[i].node1)
        node2_set_index = list_obj.index(edges[i].node2)

        if union(parents[node1_set_index], parents[node2_set_index]):
            result_set.append(edges[i].node1+edges[i].node2)
            size_tree += 1
        i += 1

    return result_set


def main():
    filename = sys.argv[-1]
    edges, list_obj = read_file(filename)
    findTree(edges, list_obj)


if __name__ == "__main__":
    main()
