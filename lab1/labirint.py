def found(pathArr, finPoint):
    result = 1
    for i in range(len(pathArr)*len(pathArr[0])):
        result += 1
        for y in range(len(pathArr)):
            for x in range(len(pathArr[0])):
                if pathArr[y][x] == (result - 1):
                    if y > 0 and pathArr[y-1][x] == 0:
                        pathArr[y-1][x] = result
                    if y < (len(pathArr)-1) and pathArr[y+1][x] == 0:
                        pathArr[y+1][x] = result
                    if x > 0 and pathArr[y][x-1] == 0:
                        pathArr[y][x-1] = result
                    if x < (len(pathArr[y])-1) and pathArr[y][x+1] == 0:
                        pathArr[y][x+1] = result
                    if (abs(y-finPoint[0]) + abs(x-finPoint[1])) == 1:
                        pathArr[finPoint[0]][finPoint[1]] = result
                        return result
    return(-1)


def printLabirint(matrix):
    for i in range(len(matrix)):
        print(matrix[i])


def main():
    matrix = []
    with open("matrix.txt") as f:
        for line in f:
            matrix.append([int(x) for x in line.split()])
    posIn = (1, 0)
    posOut = (0, 3)
    path = [[0 if x == 1 else -1 for x in y] for y in matrix]
    path[posIn[0]][posIn[1]] = 1
    print(found(path, posOut))
    return


if __name__ == '__main__':
    main()
