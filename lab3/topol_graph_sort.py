from collections import defaultdict
from graphviz import Digraph
import operator
import os
import sys

os.environ["PATH"] += os.pathsep + 'C:/Program Files (x86)/Graphviz2.38/bin/'

time_in = dict()
time_out = dict()


def dfs(graph, start, clock, visited_nodes, visited=None):
    visited_nodes.append(start)
    if visited is None:
        visited = set()
    clock += 1
    time_in[start] = clock
    visited.add(start)

    for neighbour in graph[start]:
        if neighbour in visited:
            continue
        clock, visited_nodes = dfs(graph, neighbour, clock,
                                   visited_nodes, visited)
    clock += 1
    time_out[start] = clock
    print(start, "in:", time_in[start], "out:", time_out[start])
    return clock, visited_nodes


def print_graph(sorted_graph):
    dot = Digraph()
    # reversed чтобы красиво было
    sorted_graph = list(reversed(sorted_graph))
    for node in sorted_graph:
        print(node[0])
        dot.node(str(node[0]), str(node[0]))
    dot.graph_attr["rankdir"] = "LR"
    dot.render("graph", view=True)


def read_graph(file):
    graph = defaultdict(list)
    file = open(file)
    list_nodes = []

    for line in file:
        newline = line.replace(":", "")
        list_nodes.append(list(map(int, newline.split())))

    for nodes in list_nodes:
        children = [x for i, x in enumerate(nodes) if i != 0]
        graph[nodes[0]].extend(children)

    return graph


def topological_sort(graph):
    clock = 0
    g = graph.copy()
    visited_nodes = []
    for node in graph.keys():
        if node not in visited_nodes:
            clock, visited_nodes = dfs(g, node, clock, visited_nodes)

    sorted_graph = sorted(time_out.items(), key=operator.itemgetter(1),
                          reverse=True)
    return sorted_graph


def main():
    filename = sys.argv[-1]
    graph = read_graph(filename)
    sorted_graph = topological_sort(graph)
    print_graph(sorted_graph)


if __name__ == "__main__":
    main()
