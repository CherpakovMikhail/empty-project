"""

Algorithm Prima

"""

import sys
from collections import defaultdict
from math import floor, inf


class Edge:

    def __init__(self, node1, node2, weight):
        self.node1 = node1
        self.node2 = node2
        self.weight = weight


class Node:

    def __init__(self, name, weight):
        self.name = name
        self.weight = weight


def read_file(filename):
    graph = defaultdict(list)
    edges = dict()
    with open(filename) as file:
        list_nodes = file.readline().strip().split()
        for line in file:
            node1, node2, weight = line.split()
            graph[node1].extend(node2)
            # graph[node2].extend(node1)
            edges[node1 + node2] = int(weight)
            # edges[node2 + node1] = int(weight)
    return list_nodes, graph, edges


class BinaryHeap:

    def __init__(self, list_nodes):
        self.size = 1
        self.max_size = len(list_nodes)
        self.heap = [Node(a, inf) for a in list_nodes]
        self.heap[0].weight = 0
        self.list_nodes = list_nodes
        self.prev = {i: v for v, i in enumerate(list_nodes)}

    def __contains__(self, item):
        return item in self.prev.keys()

    def __bool__(self):
        return len(self.heap) > 0

    def Parent(self, i):
        return floor(i / 2)

    def LeftChild(self, i):
        return 2 * i + 1

    def RightChild(self, i):
        return 2 * i + 2

    def sift_up(self, i):
        par_id = self.Parent(i) - 1
        while i > 1 and self.heap[par_id].weight > self.heap[i - 1].weight:
            tmp = self.heap[self.Parent(i) - 1]
            self.heap[self.Parent(i) - 1] = self.heap[i - 1]
            self.heap[i - 1] = tmp
            i = self.Parent(i)

    def sift_down(self, i):
        max_index = i
        lCh = self.LeftChild(i)
        rCh = self.RightChild(i)

        if lCh < self.size and\
                self.heap[lCh].weight < self.heap[max_index].weight:
            max_index = lCh

        if rCh < self.size and\
                self.heap[rCh].weight < self.heap[max_index].weight:
            max_index = rCh

        if i != max_index:
            tmp = self.heap[max_index]
            self.heap[max_index] = self.heap[i]
            self.heap[i] = tmp
            self.sift_down(max_index)

    def change_priority(self, i, p):
        old_p = self.heap[i].weight
        self.heap[i].weight = p
        if p < old_p:
            self.sift_up(i + 1)
        else:
            self.sift_down(i + 1)
        for i in range(len(self.heap)):
            self.list_nodes[i] = self.heap[i].name

    def extract_min(self):
        result = self.heap[0]
        self.list_nodes.remove(result.name)
        del self.heap[0]
        self.size = len(self.heap)
        self.sift_down(0)
        return result


def spanning_tree_weight(spanning_tree):
    s = 0
    for node in spanning_tree:
        s += node.weight
    return s


def prim(graph, list_nodes, edges):
    heap = BinaryHeap(list_nodes)
    visited_nodes = []
    result = []
    while heap:
        current_node = heap.extract_min()
        result.append(current_node)
        visited_nodes.append(current_node.name)
        for neighbour in graph[current_node.name]:
            if neighbour in visited_nodes:
                continue
            id_neighbour = heap.list_nodes.index(neighbour)
            neighbour_weight = edges[current_node.name+neighbour]

            if neighbour in heap and\
                    heap.heap[id_neighbour].weight > neighbour_weight:
                heap.prev[neighbour] = current_node.name
                heap.change_priority(heap.heap.index(heap.heap[id_neighbour]),
                                     neighbour_weight)

    return spanning_tree_weight(result)


def main():
    filename = sys.argv[-1]
    list_nodes, graph, edges = read_file(filename)
    prim(graph, list_nodes, edges)


if __name__ == "__main__":
    main()
