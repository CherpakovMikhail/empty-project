import operator
import sys
from collections import defaultdict
from graphviz import Digraph


time_in = dict()
time_out = dict()


def print_graph(sorted_graph):
    dot = Digraph()
    for node in sorted_graph:
        dot.node(str(node[0]), str(node[0]))
        for son in sorted_graph[node[0]]:
            dot.edge(node[0], son)
    dot.graph_attr["rankdir"] = "LR"
    dot.render("graph", view=True)


def read_graph(filename):
    graph = defaultdict(list)
    graph_t = defaultdict(list)
    with open(filename) as file:
        for line in file:
            node, *adj_nodes = line.split()
            graph[node].extend(adj_nodes)
            graph_t[adj_nodes[0]].extend(node)
    return graph, graph_t


def dfs(graph, start, clock, visited_nodes, visited=None):
    visited_nodes.append(start)
    if visited is None:
        visited = set()
    clock += 1
    time_in[start] = clock
    visited.add(start)

    for neighbour in graph[start]:
        if neighbour in visited_nodes:
            continue
        clock, visited_nodes = dfs(graph, neighbour, clock,
                                   visited_nodes, visited)
    clock += 1
    time_out[start] = clock
    return clock, visited_nodes


def topological_sort(graph):
    clock = 0
    graph_copy = graph.copy()
    visited_nodes = []
    for node in graph.keys():
        if node not in visited_nodes:
            clock, visited_nodes = dfs(graph_copy, node, clock, visited_nodes)

    sorted_graph = sorted(time_out.items(), key=operator.itemgetter(1),
                          reverse=True)
    return sorted_graph


def dfs2(graph, start, visited_nodes_in_graph_t, dict_components, count):

    if start in visited_nodes_in_graph_t:
        count -= 1
        return count

    visited_nodes_in_graph_t.add(start)
    if start not in dict_components.values():
        dict_components[str(count)].extend(start)

    for neighbour in graph[start]:
        if neighbour in visited_nodes_in_graph_t:
            continue
        count = dfs2(graph, neighbour[0], visited_nodes_in_graph_t,
                     dict_components, count)

    return count


def find_components(graph_t, sorted_graph):
    visited_nodes = set()
    dict_components = defaultdict(list)
    count = 0
    for node in sorted_graph:
        count += 1
        count = dfs2(graph_t, node[0], visited_nodes, dict_components, count)

    return len(dict_components)


def main():
    filename = sys.argv[-1]
    graph, graph_t = read_graph(filename)
    sorted_graph = topological_sort(graph)
    find_components(graph_t, sorted_graph)


if __name__ == "__main__":
    main()
