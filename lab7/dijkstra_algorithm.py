"""

Algorithm Dijkstra

"""

import sys
from collections import defaultdict
from math import floor, inf
from dataclasses import dataclass


@dataclass
class Node:
    name: str
    weight: int


def read_file(filename):
    graph = defaultdict(list)
    edges = dict()
    with open(filename) as file:
        list_nodes = file.readline().strip().split()
        for line in file:
            node1, node2, weight = line.split()
            graph[node1].extend(node2)
            edges[node1 + node2] = int(weight)

    return list_nodes, graph, edges


class BinaryHeap:

    def __init__(self, list_nodes):
        self.size = 0
        self.max_size = len(list_nodes)
        self.heap = [Node(i, inf) for i in list_nodes]
        self.heap[0].weight = 0
        self.node_id = {i: v for v, i in enumerate(list_nodes)}
        self.size += 1
        self.dist = {i: inf for v, i in enumerate(list_nodes)}

    def __contains__(self, item):
        return item in self.node_id.keys()

    def __bool__(self):
        return self.size > 0

    def parent(self, i):
        return floor(i / 2) - 1

    def leftChild(self, i):
        return 2 * i + 1

    def rightChild(self, i):
        return 2 * i + 2

    def sift_up(self, i):
        par_id = self.parent(i)
        while i > 1 and self.heap[par_id].weight > self.heap[i - 1].weight:
            tmp = self.heap[par_id]
            self.heap[par_id] = self.heap[i - 1]
            self.heap[i - 1] = tmp
            i = par_id + 1

            swap = self.node_id[tmp.name]
            self.node_id[tmp.name] = self.node_id[self.heap[par_id].name]
            self.node_id[self.heap[par_id].name] = swap

    def sift_down(self, i):
        max_index = i
        lCh = self.leftChild(i)
        rCh = self.rightChild(i)

        if lCh < self.size and\
                self.heap[lCh].weight < self.heap[max_index].weight:
            max_index = lCh

        if rCh < self.size and\
                self.heap[rCh].weight < self.heap[max_index].weight:
            max_index = rCh

        if i != max_index:
            tmp = self.heap[max_index]
            self.heap[max_index] = self.heap[i]
            self.heap[i] = tmp
            self.sift_down(max_index)

    def change_priority(self, i, p):
        old_p = self.heap[i].weight
        self.heap[i].weight = p
        if p < old_p:
            self.sift_up(i + 1)
        else:
            self.sift_down(i + 1)

    def extract_min(self):
        result = self.heap[0]
        del self.node_id[result.name]
        del self.heap[0]

        for node_name, index in self.node_id.items():
            self.node_id[node_name] -= 1

        self.size = len(self.heap)
        self.sift_down(0)
        return result


def dijkstra(graph, list_nodes, edges, start_node):
    heap = BinaryHeap(list_nodes)
    heap.dist[start_node] = 0
    visited_nodes = []
    while heap:
        current_node = heap.extract_min()
        visited_nodes.append(current_node.name)
        for neighbour in graph[current_node.name]:
            if neighbour in visited_nodes:
                continue

            neighbour_weight = edges[current_node.name + neighbour]
            if heap.dist[neighbour] > heap.dist[current_node.name] +\
                    neighbour_weight:

                heap.dist[neighbour] = heap.dist[current_node.name] +\
                                       neighbour_weight
                heap.change_priority(heap.node_id[neighbour],
                                     heap.dist[neighbour])

    return heap.dist


def main():
    filename = sys.argv[-1]
    list_nodes, graph, edges = read_file(filename)
    start_node = 'A'
    dijkstra(graph, list_nodes, edges, start_node)


if __name__ == "__main__":
    main()
