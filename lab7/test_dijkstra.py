import unittest
from dijkstra_algorithm import (
    read_file, dijkstra)


class TestSum(unittest.TestCase):

    def test__int1(self):
        # test example from presentations
        list_nodes, graph, edges = read_file("lab7/test_data/graph1.txt")
        dist = dijkstra(graph, list_nodes, edges, 'A')
        self.assertEqual(set(dist.values()), {0, 2, 3, 5, 6})

    def test__int2(self):
        list_nodes, graph, edges = read_file("lab7/test_data/graph2.txt")
        dist = dijkstra(graph, list_nodes, edges, 'A')
        self.assertEqual(set(dist.values()), {0, 1, 2, 4, 10})


if __name__ == '__main__':
    unittest.main()
