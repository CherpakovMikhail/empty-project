import sys
from collections import defaultdict


class Node:
    def __init__(self, num, data):
        self._data = data
        self._num = num
        self.left = None
        self.right = None

    def search_node(self, root, parent, num, data):
        if root is None:
            return
        if root._num == parent:
            if root.left is None:
                root.left = Node(num, data)
                return
            else:
                root.right = Node(num, data)
                return
        root.search_node(root.left, parent, num, data)
        root.search_node(root.right, parent, num, data)

    def add_node(self, root, parent, num, data):
        if root is None:
            return Node(num, data)
        root.search_node(root, parent, num, data)


def get_tree_min(root):
    if root.left is None:
        return root._data
    else:
        return get_tree_min(root.left)


def get_tree_max(root):
    if root.right is None:
        return root._data
    else:
        return get_tree_max(root.right)


def check_tree(root, left_min, right_max):
    if root is None:
        return True

    if left_min > right_max:
        return False

    if root._data < left_min or root._data > right_max:
        return False

    check_left = check_tree(root.left, left_min, root._data)
    check_right = check_tree(root.right, root._data, right_max)
    return check_left and check_right


def read_file(filename):
    root = None
    dictNodes = defaultdict(int)

    with open(filename) as f:
        N = int(next(f))
        i = 0
        for line in f:
            if i < N:
                string = [int(x) for x in line.split()]
                dictNodes[string[0]] += string[1]
                i += 1
            else:
                if root is None:
                    root = Node(0, dictNodes[0])
                edge = [int(x) for x in line.split()]
                root.add_node(root, edge[0], edge[1], dictNodes[edge[1]])
    return root


def main():
    filename = sys.argv[-1]
    root = read_file(filename)
    print(check_tree(root, get_tree_min(root),
                     get_tree_max(root)))


if __name__ == "__main__":
    main()
