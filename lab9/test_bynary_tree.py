import unittest
from check_binary_search_tree import (
    read_file, check_tree, get_tree_max, get_tree_min)


class TestSum(unittest.TestCase):

    def test__int1(self):
        root = read_file("lab9/test_data/tree1.txt")
        res = check_tree(root, get_tree_min(root), get_tree_max(root))
        self.assertEqual(res, True)

    def test__int2(self):
        root = read_file("lab9/test_data/tree2.txt")
        res = check_tree(root, get_tree_min(root), get_tree_max(root))
        self.assertEqual(res, False)

    def test__int3(self):
        root = read_file("lab9/test_data/tree3.txt")
        res = check_tree(root, get_tree_min(root), get_tree_max(root))
        self.assertEqual(res, True)

    def test__int4(self):
        root = read_file("lab9/test_data/tree4.txt")
        res = check_tree(root, get_tree_min(root), get_tree_max(root))
        self.assertEqual(res, False)


if __name__ == '__main__':
    unittest.main()
